package fr.univlille.iutinfo.m3105.q1;

public class TimerThread extends Thread {
	private final Timer TIMER;
	
	public TimerThread(Timer timer) {
		TIMER = timer;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				sleep(1000);
				TIMER.notifyObservers();
			} catch (InterruptedException e) {
				// on ignore et on espère que ce n’est pas grave
			}
		}
	}
}
