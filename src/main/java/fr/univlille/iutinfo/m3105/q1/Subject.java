package fr.univlille.iutinfo.m3105.q1;

import java.util.HashSet;
import java.util.Set;

public abstract class Subject {
	protected Set<Observer> observers;
	
	protected Subject() {
		observers = new HashSet<Observer>();
	}
	
	protected void notifyObservers() {
		for(Observer observer : observers) {
			observer.update(this);
		}
	}

	protected void notifyObservers(Object data) {
		for(Observer observer : observers) {
			observer.update(this, data);
		}
	}

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(Observer observer) {
		observers.remove(observer);
	}
}
