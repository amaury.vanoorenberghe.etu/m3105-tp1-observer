package fr.univlille.iutinfo.m3105.q1;

public class Stopwatch implements Observer {
	protected int ticks = 0;
	
	@Override
	public void update(Subject subj) {
		ticks++;
		System.out.println("[STOPWATCH] :: " + ticks);
	}
	
	@Override
	public void update(Subject subj, Object data) {
		update(subj);
	}
}
