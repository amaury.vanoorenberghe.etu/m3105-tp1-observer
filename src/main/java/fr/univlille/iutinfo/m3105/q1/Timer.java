package fr.univlille.iutinfo.m3105.q1;


public class Timer extends Subject {
	protected final TimerThread TIMER_THREAD;
	
	public Timer() {
		TIMER_THREAD = new TimerThread(this);
	}
	
	public void start() {
		TIMER_THREAD.start();
	}

	public void stopRunning() {
		TIMER_THREAD.interrupt();
	}
}
