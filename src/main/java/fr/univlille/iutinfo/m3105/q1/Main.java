package fr.univlille.iutinfo.m3105.q1;

public class Main {

	public static void main(String[] args) {
		Timer timer = new Timer();
		Observer stopwatch = new Stopwatch();
		
		timer.attach(stopwatch);
		
		timer.start();
	}
}
